    <script src="<?php echo get_template_directory_uri();?>/js/core/jquery.3.2.1.min.js?ver=1.1.0"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/core/popper.min.js?ver=1.1.0"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/core/bootstrap.min.js?ver=1.1.0"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/now-ui-kit.js?ver=1.1.0"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/aos.js?ver=1.1.0"></script>
    <script src="<?php echo get_template_directory_uri();?>/scripts/main.js?ver=1.1.0"></script>
    <?php wp_footer(); ?>
  </body>
</html>