<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Daris Baksic</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/css/aos.css?ver=1.1.0" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css?ver=1.1.0" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/css/main.css?ver=1.1.0" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" media="all" />
    <noscript>
      <style type="text/css">
        [data-aos] {
            opacity: 1 !important;
            transform: translate(0) scale(1) !important;
        }
      </style>
    </noscript>
    <?php wp_head(); ?>
  </head>
  <body id="top" <?php body_class(); ?> >
    <?php wp_body_open(); ?>