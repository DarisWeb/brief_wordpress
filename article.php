
<div class="article">
    <div class="sectiontitle">
        <h2 class="heading">Mes projets récents</h2>
    </div>
    <div id="latest" class="group">
        <?php
        while (have_posts()) : the_post(); ?>
            <article>
                
                <a class="imgover" href="<?php echo get_permalink(); ?>"><img class="img2" src="<?php echo get_the_post_thumbnail_url(); ?>" alt=""></a>
                <div class="excerpt">
                    <h6 class="title-article"><?php echo get_the_title(); ?></h6>
                    <footer><a id="voirplus" href="<?php echo get_permalink(); ?>">Voir Plus </i></a></footer>
                </div>
                <div class="entry-links"><?php wp_link_pages(); ?></div>
                
            </article>
        <?php endwhile; ?>
    </div>
</div>