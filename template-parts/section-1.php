<div class="profile-page">
  <div class="wrapper">
    <div class="page-header page-header-small" filter-color="green">
      <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo get_template_directory_uri();?>/images/web2.jpg')"></div>
      <div class="container">
        <div class="content-center">
          <div class="cc-profile-image"><a href="#"><img src="<?php echo get_template_directory_uri();?>/images/Daris.jpg" alt="Image"/></a></div>
          <div class="h2 title">Daris Baksic</div>
          <p class="category text-white">Développeur Web et Web Mobile</p><a id="btn1" class="btn btn-primary" href="<?php echo get_template_directory_uri();?>/pdf/CV Développeur Web et Web Mobile Daris Baksic.pdf" data-aos="zoom-in" data-aos-anchor="data-aos-anchor">Téléchargez mon CV</a>
        </div>
      </div>
      <div class="section">
        <!--
        <div class="container">
          <div class="button-container"><a class="btn btn-default btn-round btn-lg btn-icon" href="#" rel="tooltip" title="Follow me on Facebook"><i class="fa fa-facebook"></i></a><a class="btn btn-default btn-round btn-lg btn-icon" href="#" rel="tooltip" title="Follow me on Twitter"><i class="fa fa-twitter"></i></a><a class="btn btn-default btn-round btn-lg btn-icon" href="#" rel="tooltip" title="Follow me on Google+"><i class="fa fa-google-plus"></i></a><a class="btn btn-default btn-round btn-lg btn-icon" href="#" rel="tooltip" title="Follow me on Instagram"><i class="fa fa-instagram"></i></a></div>
        </div>
        -->
      </div>
    </div>
  </div>
</div>