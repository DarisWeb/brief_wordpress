<div class="section">
  <div class="container cc-education">
    <div class="h4 text-center mb-4 title">Formation</div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-education-header">
            <p>01/2021 - Présent</p>
            <div class="h5">Développement Web et Web Mobile</div>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">Titre de Formation Professionnelle (BAC+2)</div>
            <p class="category">Campus26 by Simplon.co, Le Puy en Velay</p>
            <p>● Développement Web<br>● HTML, CSS, JAVASCRIPT / REACT, PHP / SYMFONY, MySQL, GIT</p>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-education-header">
            <p>09/2000 - 09/2001</p>
            <div class="h5">Réseau Communication Informatique<br>(en alternance)</div>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">Certificat de Qualification Professionnelle (BAC+3)</div>
            <p class="category">Ecole La Mache, Lyon</p>
            <p>● TCP/IP, LAN, WAN, Fibre optique<br>● OS: Windows NT, Linux, Novell Netware</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
