<div class="section" id="about">
  <div class="container">
    <div class="card" data-aos="fade-up" data-aos-offset="10">
      <div class="row">
        <div class="col-lg-6 col-md-12">
          <div class="card-body">
            <div class="h4 mt-0 title">A propos</div>
            <p>Bonjour je suis Daris,</p>
            <p>Je suis actuellement la formation Développeur web et web mobile organisée par Simplon.co et Campus26.<br>
              Passionné par l'univers du web et doté d'une grande curiosité, je suis diplomé de l'école La Mache de Lyon en Réseau Communication Informatique (BAC+3). Polyvalant et fort d'une expérience significative, j'aimerais me spécialiser dans le développement et la création de sites internet, même si je suis aussi capable de faire de la maintenance informatique. Je suis aujourd'hui à la recherche de nouvelles opportunités sur la région du Puy en Velay.
              <!--<a href="https://templateflip.com/templates/creative-cv/" target="_blank">En savoir plus</a></p>-->
          </div>
        </div>
        <div class="col-lg-6 col-md-12">
          <div class="card-body">
            <div class="h4 mt-0 title">Mes informations</div>
            <div class="row">
              <div class="col-sm-4"><strong class="text-uppercase">Age:</strong></div>
              <div class="col-sm-8">42</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Email:</strong></div>
              <div class="col-sm-8">baksic.daris@gmail.com</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Téléphone:</strong></div>
              <div class="col-sm-8">07 67 38 00 43</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Adresse:</strong></div>
              <div class="col-sm-8">2 Rue Guillaume Chabalier,<br>43750 Vals près le Puy</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Language:</strong></div>
              <div class="col-sm-8">Français, Anglais, Bosnien</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>