<div class="section" id="experience">
  <div class="container cc-experience">
    <div class="h4 text-center mb-4 title">Experience professionnelle</div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-experience-header">
            <p>04/2011 - 11/2011</p>
            <div class="h5">Lycée Charles et Adrien Dupuy,<br>Le Puy en Velay</div>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">Technicien Informatique</div>
            <p>● Maintenance d'un parc de 200 ordinateurs<br>● Installation de nouveaux périphériques<br>● Câblage réseau LAN<br>● OS: Ms. Windows et Linux<br></p>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-experience-header">
            <p>09/2000 - 09/2001</p>
            <div class="h5">Recticel, Langeac</div>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">Développeur en alternance</div>
            <p>● Développement sur Ms. Access<br>● Bases de données: Oracle, Ms. Access<br>● Maitenance informatique</p>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-experience-header">
            <p>09/2005 - 10/2005</p>
            <div class="h5"> Logipro,<br>Le Puy en Velay</div>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">Stage Linux Redhat</div>
            <p>● Mise en place d'une sauvegarde réseau d'un serveur web<br>● Mise en place d'un antivirus sur serveur e-mail<br>● Outils: Shell Bash, OpenSSH, ClamAV<br></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
